all: spec_to_pinmux pinmux_to_bsv gen_verilog
	@echo making i_class

spec_to_pinmux:
	@mkdir -p build
	@cd src/pinmux/ && python src/pinmux_generator.py -s i_class \
	                                        -o ../../build/i_class
pinmux_to_bsv:
	@mkdir -p build
	@cd src/pinmux/ && python src/pinmux_generator.py \
	                                        -o ../../build/i_class
gen_verilog:
	@echo making i_class verilog
	@make -C build/i_class/bsv_src gen_verilog
