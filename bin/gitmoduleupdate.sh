#!/bin/sh
git submodule init
git submodule update
cd src/peripherals && git checkout master && git pull
cd ../pinmux && git checkout master && git pull
cd ../core && git checkout master && git pull
