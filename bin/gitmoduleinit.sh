#!/bin/sh
git submodule add gitolite3@libre-riscv.org:shakti-peripherals.git \
                        src/peripherals
git submodule add gitolite3@libre-riscv.org:pinmux.git \
                        src/pinmux
git submodule add gitolite3@libre-riscv.org:shakti-core \
                        src/core

# example how to add locally instead:
#git submodule add /home/lkcl/src/riscv/shakti-core/ src/core
